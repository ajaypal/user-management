var express = require('express');
var mysql = require('mysql');
var app = express();

app.use(express.static('/home/mandeep/public_html/user-management/ui/Contents'));
app.use(express.static('/home/mandeep/public_html/user-management/ui/Scripts'));

/****************************************************************************/
function CreateConnection()
{
    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'alphadb',
        multipleStatements: true
    });
    return connection;
}

// This responds with "Hello World" on the homepage
app.get('/', function (request, response) {
   console.log("Got a GET request for the homepage");
   response.sendFile('/home/mandeep/public_html/user-management/ui/index.html');
})
// This responds a POST request for the homepage
app.get('/newuser.html', function (request, response) {
console.log(request.query);

   //////////////CreateConnection//////////////
   var connection = CreateConnection();
   var varQuery = "insert into users set name='"+request.query.username+"', password=md5('"+request.query.password+"'), email='"+request.query.email+"',contactno='"+request.query.contactno+"',country='"+request.query.country+"',city='"+request.query.city+"'";
   console.log(varQuery);
   var varResultset = connection.query({sql: varQuery});

   //////////////Get Result: call on end////////////// 
   varResultset.on('end', function ()
   {
     //////////////End connection//////////////
      connection.end();
     ////////////////////////////////////
   });













   console.log("Got a POST request for the homepage");
   response.send('Hello POST');
})
// This responds a DELETE request for the /del_employee page.
app.delete('/del_employee', function (request, response) {
   console.log("Got a DELETE request for /del_employee");
   response.send('Hello DELETE');
})
// This responds a GET request for the /list_employee page.
app.get('/list_employee', function (request, response) {
   console.log("Got a GET request for /list_employee");
   response.send('Page Listing');
})
// This responds a GET request for abcd, abxcd, ab123cd, and so on
app.get('/ab*cd', function(request, response) {   
   console.log("Got a GET request for /ab*cd");
   response.send('Page Pattern Match');
})
var server = app.listen(3000, function () {
  var host = server.address().address
  var port = server.address().port
  console.log("Example app listening at http://%s:%s", host, port)
})
