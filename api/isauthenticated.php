<?php
require("includes/init.php");
global $_SESSION;

if(isset($_SESSION['userid']) and $_SESSION['userid'] != '')
{
  //Already Authenticated;
  echo json_encode(array('status' => 'OK', 'userid' => $_SESSION['userid'], 'usrType' => $_SESSION['usrType'], 'usrLastLogin' => $_SESSION['usrLastLogin']));
}
else
{
  echo json_encode(array('status' => 'ERROR'));
}
?>
