<?php
require("includes/init.php");
global $_SESSION;

$_SESSION['userid'] = '';
$_SESSION['usrType'] = '';
$_SESSION['usrLastLogin'] = '';
unset($_SESSION['userid']);
unset($_SESSION['usrType']);
unset($_SESSION['usrLastLogin']);

echo json_encode(array('status' => 'OK', 'usrType' => '', 'usrLastLogin' => ''));

?>
