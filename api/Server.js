var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
//var trycatch = require('trycatch');
var mysql = require('mysql');
var app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.all('/*', function(req, res, next) {
// CORS headers
    res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
// Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});
app.listen(3000, function() {
    console.log("Listening on port 3000...");
});
function CreateConnection()
{
    var connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'UserMaintenanceWeb',
        multipleStatements: true
    });
    return connection;
}
function ErrorResponse(response, returnValue)
{
    var varResponseResult = JSON.stringify({"error": returnValue});
    response.writeHead(500, {
        'Content-Length': varResponseResult.length,
        'Content-Type': 'application/json'
    });
    response.write(varResponseResult);
    response.end();
}
function SuccessResponse(response, returnValue)
{
    var varResponseResult = JSON.stringify(returnValue);
    console.log(varResponseResult);
    response.writeHead(200, {
        'Content-Length': varResponseResult.length,
        'Content-Type': 'application/json'
    });
    response.write(varResponseResult);
    response.end();
}

app.get("/api/SecurityQues.html",
        function(varRequest, response)
        {
                var varQuery = varRequest.query;
                var objData = JSON.parse(JSON.stringify(varQuery));
                var varUserId = objData.userId;
                var connection = CreateConnection();
                var varData={"status":'ERROR',"message": 'Invalid userid. Try Again'};
                //////////////Connect to connection//////////////
                connection.connect(function(err)
                {
                    //////////////Check for error//////////////
                    if (err) {
                        setTimeout(function() {
                            throw new Error('Connection Error!');
                        }, 1000);
                        ErrorResponse(response, "Connection Error");
                        return;
                    }
                    else
                    {
                        ////////////////////////////////////////
                        var varIsError = false;
                        var varQuery;
                        var varQuery = "select usrSQ from tblUser where usrId=" + connection.escape(varUserId);
			console.log(varQuery); 
                       var varResultset = connection.query({sql: varQuery});
                        varResultset.on('error', function(err)
                        {
                            // Handle error, an 'end' event will be emitted after this as well
                            setTimeout(function() {
                                throw new Error('Error!');
                            }, 1000);
                            varIsError = true;
                        });
                        varResultset.on('result', function(row)
                        {
                            connection.pause();
//                            varData.push({
//                                "SecurityQues": row.usrSQ,
//                            });
//console.log(row);
				varData={"status":'OK',"securityQuestion": row.usrSQ};
                            connection.resume();
                        });
                        varResultset.on('end', function()
                        {
                            if (varIsError == true)
                            {
                                varData = -1;
                                ErrorResponse(response, varData);
                            }
                            else
                            {
                                var returnValue = varData;
                                console.log(returnValue);
                                SuccessResponse(response, varData);
                                connection.end();
                            }
                        });
                    }
            });
        });


app.get("/api/SecurityQuesAnswer.html",
        function(varRequest, response)
        {
                var varQuery = varRequest.query;
                var objData = JSON.parse(JSON.stringify(varQuery));
                var varUserId = objData.userId;
		var varAnswer = objData.answer;
		var varSecurityQuestion = objData.securityQuestion;
                var connection = CreateConnection();
                var varData={"status":'ERROR',"message": 'Invalid Combination. Try Again'};
                //////////////Connect to connection//////////////
                connection.connect(function(err)
                {
                    //////////////Check for error//////////////
                    if (err) {
                        setTimeout(function() {
                            throw new Error('Connection Error!');
                        }, 1000);
                        ErrorResponse(response, "Connection Error");
                        return;
                    }
                    else
                    {
                        ////////////////////////////////////////
                        var varIsError = false;
                        var varQuery;
                        var varQuery = "select usrPwd from tblUser where usrId=" + connection.escape(varUserId) +" and usrSQ=" + connection.escape(varSecurityQuestion) +" and usrSA="+connection.escape(varAnswer);
			console.log(varQuery); 
                       var varResultset = connection.query({sql: varQuery});
                        varResultset.on('error', function(err)
                        {
                            // Handle error, an 'end' event will be emitted after this as well
                            setTimeout(function() {
                                throw new Error('Error!');
                            }, 1000);
                            varIsError = true;
                        });
                        varResultset.on('result', function(row)
                        {
                            connection.pause();
//                            varData.push({
//                                "SecurityQues": row.usrSQ,
//                            });
//console.log(row);
				varData={"status":'OK',"pass": row.usrPwd};
                            connection.resume();
                        });
                        varResultset.on('end', function()
                        {
                            if (varIsError == true)
                            {
                                varData = -1;
                                ErrorResponse(response, varData);
                            }
                            else
                            {
                                var returnValue = varData;
                                console.log(returnValue);
                                SuccessResponse(response, varData);
                                connection.end();
                            }
                        });
                    }
            });
        });

