<?php
require("includes/init.php");
global $_SESSION;

try 
{
  if(isset($_SESSION['userid']) and $_SESSION['userid'] != '')
  {
    //Already Authenticated;
    echo json_encode(array('status' => 'Already Authenticated, try refresing the page', 'usrType' => $_SESSION['usrType'], 'usrLastLogin' => $_SESSION['usrLastLogin']));

  }
  else
  {
    if(isset($_GET['u']) and isset($_GET['p']))
    {
      $u = $conn->quote(trim($_GET['u']));
      $p = $conn->quote(trim($_GET['p']));
      $usrType = null;
      $usrLastLogin = null;

      $data = $conn->query("SELECT usrType, usrLastLogin FROM tblUser where usrId=$u and usrPwd=$p and usrStatus='active' LIMIT 1"); 
      foreach($data as $row) {
        //print_r($row); 
        $usrType = $row['usrType'];
        $usrLastLogin = $row['usrLastLogin'];
      }
      if(!$usrType)
      {
        echo json_encode(array('status' => 'Incorrect Username or Password', 'usrType' => $usrType, 'usrLastLogin' => $usrLastLogin));
        $_SESSION['userid'] = '';
        $_SESSION['usrType'] = '';
        $_SESSION['usrLastLogin'] = '';
        unset($_SESSION['userid']);
        unset($_SESSION['usrType']);
        unset($_SESSION['usrLastLogin']);
      }
      else
      {
        $_SESSION['userid'] = $u;
        $_SESSION['usrType'] = $usrType;
        $_SESSION['usrLastLogin'] = $usrLastLogin;

        $conn->query("update tblUser set usrLastLogin=NOW() where usrId=$u");
        echo json_encode(array('status' => 'OK', 'usrType' => $usrType, 'usrLastLogin' => $usrLastLogin));
      }
    }
    else
    {
      echo json_encode(array('status' => "User ID or Password is blank", 'usrType' => '', 'usrLastLogin' => ''));
    }
  }
}
catch(PDOException $e) 
{
  echo json_encode(array('status' => $e->getMessage(), 'usrType' => '', 'usrLastLogin' => ''));
}
?>
