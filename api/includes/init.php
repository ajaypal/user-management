<?php
session_name("SPA-TEST");
session_start();
$conn = null;

try 
{
  $conn = new PDO('mysql:host=localhost;dbname=UserMaintenanceWeb', 'root', 'root');
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e) 
{
  echo 'ERROR: ' . $e->getMessage();
  exit(1);
}
?>
