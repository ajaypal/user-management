<?php
require("includes/init.php");
global $_SESSION;

$menu = null;
if(isset($_SESSION['userid']) and $_SESSION['userid'] != '')
{
  switch($_SESSION['usrType'])
  {
    case "admin":
      $menu = array( 
          array('lable' => 'Home of Admin', 'url' => '/adminhome'), 
          array('lable' => 'Logout', 'url' => '/logout')
      );
      break;
    case "user";
      $menu = array( 
          array('lable' => 'Home of User', 'url' => '/userhome'), 
          array('lable' => 'Logout', 'url' => '/logout')
      );
      break;
  }
}
else //guest user
{
  $menu = array(
    array('lable' => 'Home', 'url' => '/'), 
    array('lable' => 'Login', 'url' => '/login'), 
    array('lable' => 'Signup', 'url' => '/signup'),
    array('lable' => 'Forgot Password', 'url' => '/forgotpassword')
  );
}
echo json_encode($menu);
?>
