DROP SCHEMA IF EXISTS `UserMaintenanceWeb` ;
CREATE SCHEMA IF NOT EXISTS `UserMaintenanceWeb` ;
USE `UserMaintenanceWeb` ;

-- -----------------------------------------------------
-- Table `tblUserSQ`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tblUserSQ` (
  `usrSQ` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`usrSQ`) );


-- -----------------------------------------------------
-- Table `tblGICountry`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tblGICountry` (
  `countryId` INT NOT NULL AUTO_INCREMENT ,
  `countryName` VARCHAR(100),
  `countryStatus` BIT,
  PRIMARY KEY (`countryId`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tblGIState`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tblGIState` (
  `stateId` INT NOT NULL AUTO_INCREMENT ,
  `stateName` VARCHAR(100),
  `countryId` INT,
  `stateStatus` BIT,
  PRIMARY KEY (`stateId`) ,
  CONSTRAINT `FK_tblGIState_countryId` FOREIGN KEY (`countryId` ) 
				REFERENCES `tblGICountry` (`countryId`) ON DELETE cascade ON UPDATE cascade
	)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `tblGICity`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tblGICity` (
  `cityId` INT NOT NULL AUTO_INCREMENT ,
  `cityName` VARCHAR(100),
  `stateId` INT,
  `cityStatus` BIT,
  PRIMARY KEY (`cityId`) ,
  CONSTRAINT `FK_tblGICity_stateId` FOREIGN KEY (`stateId`) 
				REFERENCES `tblGIState` (`stateId`) ON DELETE cascade ON UPDATE cascade
	)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `tblUser`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tblUser` (
  `usrId` VARCHAR(10) NOT NULL ,
  `usrPwd` VARCHAR(10),
  `usrSQ` VARCHAR(200),
  `usrSA` VARCHAR(50),
  `usrType` VARCHAR(10),
  `usrLastLogin` DATETIME,
  `usrStatus` VARCHAR(10),
  PRIMARY KEY (`usrId`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tblUserDetail`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tblUserDetail` (
  `usrId` VARCHAR(10) NOT NULL ,
  `usrName` VARCHAR(100),
  `usrDOB` DATETIME,
  `usrGender` VARCHAR(10),
  `usrEmail` VARCHAR(100),
  `usrStreet` VARCHAR(100),
  `usrCountry` INT,
  `usrState` INT,
  `usrCity` INT,
  `usrPIN` VARCHAR(10),
  `usrMobile` VARCHAR(10),
  PRIMARY KEY (`usrId`) ,
  CONSTRAINT `FK_tblUserDetail_usrId` FOREIGN KEY (`usrId`) 
				REFERENCES `tblUser` (`usrId`),
  CONSTRAINT `FK_tblUserDetail_usrCountry` FOREIGN KEY (`usrCountry`)
				REFERENCES `tblGICountry` (`countryId`) ON DELETE no action ON UPDATE no action,
  CONSTRAINT `FK_tblUserDetail_usrState` FOREIGN KEY (`usrState`) 
				REFERENCES `tblGIState` (`stateId` )  ON DELETE no action ON UPDATE no action,
  CONSTRAINT `FK_tblUserDetail_usrCity` FOREIGN KEY (`usrCity` )
				REFERENCES `tblGICity` (`cityId` )  ON DELETE no action ON UPDATE no action
	)
ENGINE = InnoDB;


insert into tblUserSQ (usrSQ) values
('Who is your childhood hero?'),
('What is your pets name?'), 
('What is your favorite song?'),
('What is your place of birth?');

insert into tblGICountry (countryId,countryName,countryStatus)values
(1,'India',1),
(2,'USA',1),
(3,'UK',1),
(4,'Nepal',1),
(5,'Sri Lanka',1),
(6,'Bhutan',1),
(7,'Tibet',1),
(8,'Bangladesh',1),
(9,'Pakistan',0);

insert into tblGIState (stateId,stateName,countryId,stateStatus)values
(1,'Punjab',1,1),
(2,'Himachal Pradesh',1,1),
(3,'Gujarat',1,1),
(4,'Haryana',1,1),
(5,'Jammu and Kashmir',1,1),
(6,'Kerala',1,1),
(7,'Mizoram',1,1),
(8,'Rajasthan',1,1),
(9,'Tripura',1,1),
(10,'West Bengal',1,1),
(11,'Central',5,1),
(12,'Eastern',5,1),
(13,'North Central',5,1),
(14,'Northern',5,1),
(15,'North Western',5,1),
(16,'Sabaragamuwa',5,1),
(17,'Southern',5,1),
(18,'Uva',5,1),
(19,'Western',5,1),
(20,'Alabama',2,1),
(21,'Alaska',2,1),
(22,'California',2,1),
(23,'Colorado',2,1),
(24,'Florida',2,1),
(25,'Georgia',2,1),
(26,'Hawaii',2,1),
(27,'Indiana',2,1),
(28,'Maryland',2,1),
(29,'New Jersey',2,1),
(30,'New Mexico',2,1),
(31,'New York',2,1),
(32,'Pennsylvania',2,1),
(33,'Texas',2,1),
(34,'Virginia',2,1),
(35,'Washington',2,1);

insert into tblGICity (cityId,cityName,stateId,cityStatus)values
(1,'Chandigarh',1,1),
(2,'Amritsar',1,1),
(3,'Barnala',1,1),
(4,'Bathinda',1,1),
(5,'Faridkot',1,1),
(6,'Fazilka',1,1),
(7,'Hoshiarpur',1,1),
(8,'Jalandhar',1,1),
(9,'Ludhiana',1,1),
(10,'Pathankot',1,1),
(11,'Rupnagar',1,1),
(12,'Ajitgarh (Mohali)',1,1),
(13,'Shimla',2,1),
(14,'Kangra',2,1),
(15,'Hamirpur',2,1)  ; 

insert into tblUser (usrId,usrPwd,usrSQ,usrSA,usrType,usrLastLogin,usrStatus)values
('admin','admin','Who is your childhood hero?','superman','admin',null,'active'),
('u101','user1','Who is your childhood hero?','heman','user',null,'active'),
('u102','user2','What is your pets name?','pepper','user',null,'active'),
('u103','user3','What is your place of birth?','delhi','user',null,'active');

insert into tblUserDetail(usrId,usrName,usrDOB,usrGender,usrEmail,usrStreet,usrCountry,usrState,usrCity,usrPIN,usrMobile)values
('admin','Matin','1987-01-01','Male','martin@yahoo.com','#145 Sector 90A',1,1,1,'160090','9541256894'),
('u101','Harry','1986-09-05','Male','harry101@gmail.com','#25 Sector 10A',1,1,1,'160010','8745874589'),
('u102','Jestin','1984-11-09','Male','justin91@gmail.com','#91 Sector 15A',1,1,1,'160015','7895698546'),
('u103','Maria','1983-07-06','Female','maria198@ymail.com','#88 Sector 25A',1,1,1,'160025','7325489568');
