var app=angular.module('single-page-app',['ngRoute']);
app.run(function($rootScope) {
  $rootScope.usrLastLogin = "";
  $rootScope.NodeAPIServerURL = "http://localhost:3000";
});

app.config(function($routeProvider){
  $routeProvider
    .when('/',{
      templateUrl: 'guesthome.html',
      controller: 'mainController'
    })
    .when('/adminhome',{
      templateUrl: 'adminhome.html',
      controller: 'mainController'
    })
    .when('/userhome',{
      templateUrl: 'userhome.html',
      controller: 'mainController'
    })
    .when('/login',{
      templateUrl: 'login.html',
      controller: 'loginController'
    })
    .when('/forgotpassword',{
      templateUrl: 'forgotpassword.html',
      controller: 'forgotPasswordController'
    })
    .when('/signup',{
      templateUrl: 'signup.html'
    })
    .when('/logout',{
      templateUrl: 'logout.html',
      controller: 'logoutController'
    })
    .otherwise({
      templateUrl: '404.html'
    })
});

