app.controller('mainController', function($scope, $http, $location, $rootScope) {

  angular.element(document).ready(function () {
    $rootScope.isAuthenticated();
    $rootScope.getMenu();
  });

  $rootScope.isAuthenticated = function () {
    $http.get("../api/isauthenticated.php")
    .success(function(response) {
      if(response.status=="OK")
      {
        $rootScope.usrID = response.userid;
        $rootScope.usrType = response.usrType;
        $rootScope.usrLastLogin = response.usrLastLogin;
        $rootScope.authenticated = true;
        //alert(response.status);
        //do we need to redirect?
        //$location.path("/adminhome");
        return true;
      }
      else
      {
        $rootScope.usrID = "";
        $rootScope.usrType = "";
        $rootScope.usrLastLogin = "";
        $rootScope.authenticated = false;

        return false;
      }
    });
  };

  $rootScope.getMenu = function () {
    $http.get("../api/getMainMenu.php")
    .success(function (response) {$rootScope.mainMenu = response;});
  }
});

