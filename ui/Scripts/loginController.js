app.controller('loginController', function($scope, $http, $location, $rootScope) {
  $scope.processLogin = function () {
    var userid = $scope.userid;
    var password = $scope.password;
    //var varData = JSON.stringify({ "Id": userid, "Pass": password });
    //alert("Data is: "+ varData);
    $http.get("../api/login.php?u="+userid+"&p="+password)
    .success(function(response) {
      if(response.status=="OK")
      {
        $rootScope.usrType = response.usrType;
        $rootScope.usrLastLogin = response.usrLastLogin;
        $rootScope.authenticated = true;

        //alert(response.status);
        //$location.path("/adminhome");
        $rootScope.getMenu();

        switch($rootScope.usrType)
        {
          case 'admin':
            $location.path("/adminhome");
            break;
          case 'user':
            $location.path("/userhome");
            break;
          //default:
            //$location.path("/");
            //break;
        }
      }
      else
      {
        $rootScope.usrType = "";
        $rootScope.usrLastLogin = "";
        $rootScope.authenticated = false;

        alert("Error: "+response.status);
      }
    });
  }
});

