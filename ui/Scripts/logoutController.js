app.controller('logoutController', function($scope, $http, $location, $rootScope) {
  $scope.$on('$viewContentLoaded', function(){
    //When view content is loaded
    $scope.processLogout();
  });

  $scope.processLogout = function () {
    //alert(1);
    $http.get("../api/logout.php")
    .success(function(response) {
      if(response.status=="OK")
      {
        $rootScope.usrType = "";
        $rootScope.usrLastLogin = "";
        $rootScope.authenticated = false;

        //alert(response.status);
        $location.path("/login");
        $rootScope.getMenu();
      }
      else
      {
        $rootScope.usrType = "";
        $rootScope.usrLastLogin = "";
        $rootScope.authenticated = false;

        alert("Error: "+response.status);
        $location.path("/login");
      }
    });
  }

});

