app.controller('forgotPasswordController', function($scope, $http, $location, $rootScope) {

  $scope.getQuestionForUserID = function() {
    //alert($scope.forgotuserid);
    $http.defaults.useXDomain = true;
    //$http.defaults.withCredentials = false;
    delete $http.defaults.headers.common['X-Requested-With'];

    $http.get($rootScope.NodeAPIServerURL+"/api/SecurityQues.html?userId="+$scope.forgotuserid)
    .success(function(response) {
      if(response.status=="OK")
      {
        $scope.secQuestion = response.securityQuestion;
      }
      else
      {
        alert("Error: "+response.message);
      }
    })
    .error(function(response) {
      alert('Try again, Unable to connect to API Server ' + $rootScope.NodeAPIServerURL);
    });
  };

  $scope.getPassForUserIDandAnswer = function() {
    //alert($scope.forgotuserid);
    $http.defaults.useXDomain = true;
    //$http.defaults.withCredentials = false;
    delete $http.defaults.headers.common['X-Requested-With'];

    $http.get($rootScope.NodeAPIServerURL+"/api/SecurityQuesAnswer.html?userId="+$scope.forgotuserid+"&securityQuestion="+$scope.secQuestion+"&answer="+$scope.secAnswer)
    .success(function(response) {
      if(response.status=="OK")
      {
        $scope.recoveredPassword = "Your password: "+response.pass;
      }
      else
      {
        alert("Error: "+response.message);
      }
    })
    .error(function(response) {
      alert('Try again, Unable to connect to API Server ' + $rootScope.NodeAPIServerURL);
    });
  };
});
